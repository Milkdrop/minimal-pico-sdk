#include "pico/stdlib.h"
#include "pico/pico4ml/screen.h"
#include "pico/stdio.h"
#include <stdio.h>
#include "pico/multicore.h"

void core1_entry () {
    int i = 0;

    while (true) {
        printf("I am core 1! This is my #%d hello!\n", i++);
        sleep_ms(1000);
    }
}

int main() {
    stdio_init_all();
    setvbuf ( stdin , NULL , _IONBF , 0 );
    setvbuf ( stdout , NULL , _IONBF , 0 );

    // These lines are relevant to you only if you have a Pico4ML
    //ST7735_Init();  // Initialise the screen
    //ST7735_FillScreen(ST7735_RED);

    multicore_launch_core1(core1_entry);

    // All of the original pico SDK demos should work
    int i = 0;

    while (true) {
        printf("Hello! This is echo #%d\n", i++);
        sleep_ms(250);
    }
    
    return 0;
}
