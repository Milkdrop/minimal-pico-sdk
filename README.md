# What is this?

This is an example "Hello World" project that uses a **minimal SDK** that should be **fully compatible** with the original [Raspberry Pi Pico SDK](https://github.com/raspberrypi/pico-sdk).

# What are some benefits of this minimal SDK?
- Has the full set of features from the original Raspberry Pi Pico SDK
- Size reduced from **2.3 GB** to **4.8 MB**
- Number of lines of code reduced from **5,003,082** to **8,572** (excluding comments)
- Number of CMake files reduced from **157** to **1**
- Very easy to tinker with: It's small, it's complete (it also compiles the second-stage bootloader) and you can easily modify any part of it to your liking, or you can simply read through it to see how the Raspberry Pi Pico works

# How do I use it?
- The entire sdk is in the "pico" folder
- You can copy that folder into any project and import things from there (like pico/stdio.h, pico/multicore.h, etc...)
- It's fully self contained and it doesn't need any extra configuration! It's plug and play and ready to use anywhere.

# Gotchas
- As of now the SDK is bundled in its entirety into your application, which can result in quite large binaries. Someone smarter could probably make a CMake config file which selectively picks which parts of the SDK are actually being used.
