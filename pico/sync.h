/*
 * Copyright (c) 2020 Raspberry Pi (Trading) Ltd.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _PICO_SYNC_H
#define _PICO_SYNC_H

/** \file sync.h
 *  \defgroup pico_sync pico_sync
 * Synchronization primitives and mutual exclusion
 */

#include "sem.h"
#include "mutex.h"
#include "critical_section.h"

#endif
